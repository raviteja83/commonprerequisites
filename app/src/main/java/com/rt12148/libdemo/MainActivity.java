package com.rt12148.libdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rt12148.commonprerequisites.widgets.AspectImageView;
import com.rt12148.commonprerequisites.widgets.CircleImageView;
import com.rt12148.commonprerequisites.widgets.SquareImageView;
import com.rt12148.commonprerequisites.widgets.UniversalLayout;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private UniversalLayout mUniversalLayout;
    private List<String> mData = new ArrayList<>();
    private Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUniversalLayout = (UniversalLayout) findViewById(R.id.universal_layout);
        for(int i =0; i< 10;i++){
            mData.add("item "+i);
        }
        mUniversalLayout.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new Adapter();
        mAdapter.setViewType(Adapter.VIEW_TEXT);
        mUniversalLayout.setAdapter(mAdapter);
        /** hide progress and set error false **/
        mUniversalLayout.hideProgress(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                changeAdapter(Adapter.VIEW_ASPECT);
                break;
            case R.id.item2:
                changeAdapter(Adapter.VIEW_CIRCULAR);
                break;
            case R.id.item3:
                changeAdapter(Adapter.VIEW_SQUARE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeAdapter(int viewType){
        mUniversalLayout.hideProgress(false);
        if(viewType == Adapter.VIEW_ASPECT){
            StaggeredGridLayoutManager lm = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
            lm.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
            mUniversalLayout.setLayoutManager(lm);
        }else {
            mUniversalLayout.setLayoutManager(new GridLayoutManager(this, 2));
        }
        addImageData();
        mAdapter.setViewType(viewType);
        mAdapter.notifyDataSetChanged();
    }

    private void addImageData(){
        mData.clear();
        mAdapter.notifyDataSetChanged();
        mData.add("http://img.deseretnews.com/images/top/landmain/3556/3556.jpg");
        mData.add("http://cdn.movieweb.com/img.teasers.posters/FI8tfcbhGPl8ah_315_a.jpg");
        mData.add("http://cdn.movieweb.com/img.teasers.posters/FIooCxsu27f7qx_317_a.jpg");
        mData.add("https://www.macxdvd.com/mac-dvd-video-converter-how-to/article-image/best-movies-2016-1.jpg");
        mData.add("http://movies.upcomingdate.com/wp-content/uploads/2015/12/Popeye-2016.jpg");
        mData.add("http://cdn.movieweb.com/img.teasers.posters/FIdkwwifsiPJgi_324_a.jpg");
    }

    private class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

        public  static  final int VIEW_TEXT = 0;
        public  static  final int VIEW_ASPECT = 1;
        public  static  final int VIEW_CIRCULAR = 2;
        public  static  final int VIEW_SQUARE = 3;

        private int viewType = 0;

        public Adapter() {
        }

        public void setViewType(int viewType) {
            this.viewType = viewType;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int layout;
            switch (viewType){
                case VIEW_TEXT:
                    layout = R.layout.list_row_item;
                    break;
                case VIEW_ASPECT:
                    layout = R.layout.list_row_aspect_imageview;
                    break;
                case VIEW_CIRCULAR:
                    layout = R.layout.list_row_circle_imageview;
                    break;
                case VIEW_SQUARE:
                    layout = R.layout.list_row_square_imageview;
                    break;
                default:
                    layout = R.layout.list_row_item;
                    break;
            }
            View view = getLayoutInflater().inflate(layout,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            switch (viewType){
                case VIEW_TEXT:
                    holder.textView.setText(mData.get(position));
                    break;
                case VIEW_ASPECT:
                    Glide.with(MainActivity.this)
                            .load(mData.get(position))
                            .error(R.color.colorAccent)
                            .into(holder.aspectImageView);
                    break;
                case VIEW_CIRCULAR:
                    Glide.with(MainActivity.this)
                            .load(mData.get(position))
                            .error(R.color.colorAccent)
                            .into(holder.circleImageView);
                    break;
                case VIEW_SQUARE:
                    Glide.with(MainActivity.this)
                            .load(mData.get(position))
                            .asBitmap()
                            .error(R.color.colorAccent)
                            .into(holder.squareImageView);
                    break;
                default:
                    holder.textView.setText(mData.get(position));
                    break;

            }
        }

        @Override
        public int getItemViewType(int position) {
            return viewType;
        }

        @Override
        public int getItemCount() {
            return mData == null ? 0 : mData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView textView;
            CircleImageView circleImageView;
            AspectImageView aspectImageView;
            SquareImageView squareImageView;

            public ViewHolder(View itemView) {
                super(itemView);
                switch (viewType){
                    case VIEW_TEXT :
                        textView = (TextView) itemView;
                        break;
                    case VIEW_ASPECT:
                        aspectImageView = (AspectImageView) itemView;
                        break;
                    case VIEW_CIRCULAR:
                        circleImageView = (CircleImageView) itemView;
                        break;
                    case VIEW_SQUARE:
                        squareImageView = (SquareImageView) itemView;
                        break;
                    default:
                        textView = (TextView) itemView;
                        break;
                }
            }
        }
    }
}
