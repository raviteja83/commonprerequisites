###The repo consists of the following :###
* Recyclerview with swipe refresh,empty view and retry options
* AspectImageView(preserve aspect ratio while loading image to ImageView)
* TopCropImageView(similar to centerCrop of imageview but the top portion of imageview      is displayed)
* VerticalViewPager(custom viewpager which can be swiped vertically)
* CircleImageView(circular imageview with border and overlay) 
* SquareImageView(imageview with equal width and height and rounded corners)

### How do I get set up? ###
  Add as following dependency to your project

  **Maven**
     
```
#!xml
<dependency>
  <groupId>com.rt12148</groupId>
  <artifactId>commonprerequisites</artifactId>
  <version>0.0.1</version>
  <type>pom</type>
</dependency>
```

 **Gradle**

```
#!java

compile 'com.rt12148:commonprerequisites:0.0.1'

```
 
### Who do I talk to? ###

* @raviteja83