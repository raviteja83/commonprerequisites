package com.rt12148.commonprerequisites.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.rt12148.commonprerequisites.R;

/**
 * Created by Ravi-PC on 05-07-2016.
 **/
public class SquareImageView extends ImageView {
    private float mBorderRadius;
    public SquareImageView(Context context) {
        this(context,null);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SquareImageView, defStyle, 0);
            try {
                mBorderRadius = a.getDimension(R.styleable.SquareImageView_cornerRadius, 8);
            } finally {
                a.recycle();
            }
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); // Snap to width
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(getResources(),bm);
        dr.setCornerRadius(mBorderRadius);
        setImageDrawable(dr);
    }
}

