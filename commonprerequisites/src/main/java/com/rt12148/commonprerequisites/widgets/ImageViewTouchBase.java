package com.rt12148.commonprerequisites.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.rt12148.commonprerequisites.BuildConfig;

import it.sephiroth.android.library.easing.Cubic;
import it.sephiroth.android.library.easing.Easing;

public abstract class ImageViewTouchBase extends ImageView{

    public enum DisplayType {
        FIT_TO_SCREEN,
        FIT_IF_BIGGER
    }

    static final String LOG_TAG = "ImageViewTouchBase";
    static final boolean LOG_ENABLED = BuildConfig.DEBUG;

    private static final float ZOOM_INVALID = - 1f;

    private final Easing mEasing = new Cubic();
    private final Matrix mBaseMatrix = new Matrix();
    private final Matrix mSuppMatrix = new Matrix();
    private Matrix mNextMatrix;
    private final Handler mHandler = new Handler();
    private Runnable mLayoutRunnable = null;
    boolean mUserScaled = false;

    private float mMaxZoom = ZOOM_INVALID;
    private float mMinZoom = ZOOM_INVALID;

    // true when min and max zoom are explicitly defined
    private boolean mMaxZoomDefined;
    private boolean mMinZoomDefined;

    private final Matrix mDisplayMatrix = new Matrix();
    private final float[] mMatrixValues = new float[9];

    private int mThisWidth = - 1;
    private int mThisHeight = - 1;
    private final PointF mCenter = new PointF();

    private final DisplayType mScaleType = DisplayType.FIT_IF_BIGGER;
    private boolean mScaleTypeChanged;
    private boolean mBitmapChanged;

    final int DEFAULT_ANIMATION_DURATION = 200;

    private final RectF mBitmapRect = new RectF();
    private final RectF mCenterRect = new RectF();
    private final RectF mScrollRect = new RectF();

    public ImageViewTouchBase(Context context) {
        this(context, null);
    }

    public ImageViewTouchBase(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageViewTouchBase(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    boolean getBitmapChanged() {
        return mBitmapChanged;
    }

    void init(Context context, AttributeSet attrs, int defStyle) {
        setScaleType(ScaleType.MATRIX);
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        if (scaleType == ScaleType.MATRIX) {
            super.setScaleType(scaleType);
        }
        else {
            Log.w(LOG_TAG, "Unsupported scaleType. Only MATRIX can be used");
        }
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

        if (LOG_ENABLED) {
            Log.e(LOG_TAG, "onLayout: " + changed + ", bitmapChanged: " + mBitmapChanged + ", scaleChanged: " + mScaleTypeChanged);
        }

        super.onLayout(changed, left, top, right, bottom);

        int deltaX = 0;
        int deltaY = 0;

        if (changed) {
            int oldw = mThisWidth;
            int oldh = mThisHeight;

            mThisWidth = right - left;
            mThisHeight = bottom - top;

            deltaX = mThisWidth - oldw;
            deltaY = mThisHeight - oldh;

            // update center point
            mCenter.x = mThisWidth / 2f;
            mCenter.y = mThisHeight / 2f;
        }

        Runnable r = mLayoutRunnable;

        if (r != null) {
            mLayoutRunnable = null;
            r.run();
        }

        final Drawable drawable = getDrawable();

        if (drawable != null) {

            if (changed || mScaleTypeChanged || mBitmapChanged) {

                if (mBitmapChanged) {
                    mBaseMatrix.reset();
                    if (! mMinZoomDefined) mMinZoom = ZOOM_INVALID;
                    if (! mMaxZoomDefined) mMaxZoom = ZOOM_INVALID;
                }

                float scale = 1;

                // retrieve the old values
                float old_matrix_scale = getScale(mBaseMatrix);
                float old_scale = getScale();
                float old_min_scale = Math.min(1f, 1f / old_matrix_scale);

                getProperBaseMatrix(drawable, mBaseMatrix);

                float new_matrix_scale = getScale(mBaseMatrix);

                if (LOG_ENABLED) {
                    Log.d(LOG_TAG, "old matrix scale: " + old_matrix_scale);
                    Log.d(LOG_TAG, "new matrix scale: " + new_matrix_scale);
                    Log.d(LOG_TAG, "old min scale: " + old_min_scale);
                    Log.d(LOG_TAG, "old scale: " + old_scale);
                }

                // 1. bitmap changed or scaleType changed
                if (mBitmapChanged || mScaleTypeChanged) {

                    if (LOG_ENABLED) {
                        Log.d(LOG_TAG, "display type: " + mScaleType);
                        Log.d(LOG_TAG, "newMatrix: " + mNextMatrix);
                    }

                    if (mNextMatrix != null) {
                        mSuppMatrix.set(mNextMatrix);
                        mNextMatrix = null;
                        scale = getScale();
                    }
                    else {
                        mSuppMatrix.reset();
                        scale = getDefaultScale(mScaleType);
                    }

                    setImageMatrix(getImageViewMatrix());

                    if (scale != getScale()) {
                        if (LOG_ENABLED) {
                            Log.v(LOG_TAG, "scale != getScale: " + scale + " != " + getScale());
                        }
                        zoomTo(scale);
                    }

                }
                else if (changed) {

                    // 2. layout size changed

                    if (! mMinZoomDefined) mMinZoom = ZOOM_INVALID;
                    if (! mMaxZoomDefined) mMaxZoom = ZOOM_INVALID;

                    setImageMatrix(getImageViewMatrix());
                    postTranslate(- deltaX, - deltaY);


                    if (! mUserScaled) {
                        scale = getDefaultScale(mScaleType);
                        if (LOG_ENABLED) {
                            Log.v(LOG_TAG, "!userScaled. scale=" + scale);
                        }
                        zoomTo(scale);
                    }
                    else {
                        if (Math.abs(old_scale - old_min_scale) > 0.001) {
                            scale = (old_matrix_scale / new_matrix_scale) * old_scale;
                        }
                        if (LOG_ENABLED) {
                            Log.v(LOG_TAG, "userScaled. scale=" + scale);
                        }
                        zoomTo(scale);
                    }
                }

                mUserScaled = false;

                if (scale > getMaxScale() || scale < getMinScale()) {
                    // if current scale if outside the min/max bounds
                    // then restore the correct scale
                    zoomTo(scale);
                }

                center();

                if (mBitmapChanged) onDrawableChanged();
                if (changed || mBitmapChanged || mScaleTypeChanged) onLayoutChanged();

                if (mScaleTypeChanged) mScaleTypeChanged = false;
                if (mBitmapChanged) mBitmapChanged = false;

                if (LOG_ENABLED) {
                    Log.d(LOG_TAG, "new scale: " + getScale());
                }
            }
        }
        else {
            // drawable is null
            if (mBitmapChanged) onDrawableChanged();
            if (changed || mBitmapChanged || mScaleTypeChanged) onLayoutChanged();

            if (mBitmapChanged) mBitmapChanged = false;
            if (mScaleTypeChanged) mScaleTypeChanged = false;

        }
    }

    /*public void resetDisplay() {
        mBitmapChanged = true;
        requestLayout();
    }

    public void resetMatrix() {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "resetMatrix");
        }
        mSuppMatrix = new Matrix();

        float scale = getDefaultScale(mScaleType);
        setImageMatrix(getImageViewMatrix());

        if (LOG_ENABLED) {
            Log.d(LOG_TAG, "default scale: " + scale + ", scale: " + getScale());
        }

        if (scale != getScale()) {
            zoomTo(scale);
        }

        postInvalidate();
    }*/

    private float getDefaultScale(DisplayType type) {
        if (type == DisplayType.FIT_TO_SCREEN) {
            // always fit to screen
            return 1f;
        }
        else if (type == DisplayType.FIT_IF_BIGGER) {
            // normal scale if smaller, fit to screen otherwise
            return Math.min(1f, 1f / getScale(mBaseMatrix));
        }
        else {
            // no scale
            return 1f / getScale(mBaseMatrix);
        }
    }

    @Override
    public void setImageResource(int resId) {
        setImageDrawable(ContextCompat.getDrawable(getContext(),resId));
    }

    @Override
    public void setImageBitmap(final Bitmap bitmap) {
        setBitmapImage(bitmap);
    }

    private void setBitmapImage(final Bitmap bitmap) {
        if (bitmap != null) setImageDrawable(new FastBitmapDrawable(bitmap), null, ImageViewTouchBase.ZOOM_INVALID, ImageViewTouchBase.ZOOM_INVALID);
        else setImageDrawable(null, null, ImageViewTouchBase.ZOOM_INVALID, ImageViewTouchBase.ZOOM_INVALID);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        setImageDrawable(drawable, null, ZOOM_INVALID, ZOOM_INVALID);
    }

    private void setImageDrawable(final Drawable drawable, final Matrix initial_matrix, final float min_zoom, final float max_zoom) {
        final int viewWidth = getWidth();

        if (viewWidth <= 0) {
            mLayoutRunnable = new Runnable() {

                @Override
                public void run() {
                    setImageDrawable(drawable, initial_matrix, min_zoom, max_zoom);
                }
            };
            return;
        }
        _setImageDrawable(drawable, initial_matrix, min_zoom, max_zoom);
    }

    void _setImageDrawable(final Drawable drawable, final Matrix initial_matrix, float min_zoom, float max_zoom) {

        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "_setImageDrawable");
        }

        mBaseMatrix.reset();

        if (drawable != null) {
            if (LOG_ENABLED) {
                Log.d(LOG_TAG, "size: " + drawable.getIntrinsicWidth() + "x" + drawable.getIntrinsicHeight());
            }
            super.setImageDrawable(drawable);
        }
        else {
            super.setImageDrawable(null);
        }

        if (min_zoom != ZOOM_INVALID && max_zoom != ZOOM_INVALID) {
            min_zoom = Math.min(min_zoom, max_zoom);
            max_zoom = Math.max(min_zoom, max_zoom);

            mMinZoom = min_zoom;
            mMaxZoom = max_zoom;

            mMinZoomDefined = true;
            mMaxZoomDefined = true;

            if (mScaleType == DisplayType.FIT_TO_SCREEN || mScaleType == DisplayType.FIT_IF_BIGGER) {

                if (mMinZoom >= 1) {
                    mMinZoomDefined = false;
                    mMinZoom = ZOOM_INVALID;
                }

                if (mMaxZoom <= 1) {
                    mMaxZoomDefined = true;
                    mMaxZoom = ZOOM_INVALID;
                }
            }
        }
        else {
            mMinZoom = ZOOM_INVALID;
            mMaxZoom = ZOOM_INVALID;

            mMinZoomDefined = false;
            mMaxZoomDefined = false;
        }

        if (initial_matrix != null) {
            mNextMatrix = new Matrix(initial_matrix);
        }

        if (LOG_ENABLED) {
            Log.v(LOG_TAG, "mMinZoom: " + mMinZoom + ", mMaxZoom: " + mMaxZoom);
        }

        mBitmapChanged = true;
        requestLayout();
    }

    private void onDrawableChanged() {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "onDrawableChanged");
            Log.v(LOG_TAG, "scale: " + getScale() + ", minScale: " + getMinScale());
        }
//        fireOnDrawableChangeListener(drawable);
    }

   /* protected void fireOnLayoutChangeListener(int left, int top, int right, int bottom) {
        if (null != mOnLayoutChangeListener) {
            mOnLayoutChangeListener.onLayoutChanged(left, top, right, bottom);
        }
    }

    protected void fireOnDrawableChangeListener(Drawable drawable) {
        if (null != mDrawableChangeListener) {
            mDrawableChangeListener.onDrawableChanged(drawable);
        }
    }*/

    private void onLayoutChanged() {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "onLayoutChanged");
        }
//        fireOnLayoutChangeListener(left, top, right, bottom);
    }

    private float computeMaxZoom() {
        final Drawable drawable = getDrawable();

        if (drawable == null) {
            return 1F;
        }

        float fw = (float) drawable.getIntrinsicWidth() / (float) mThisWidth;
        float fh = (float) drawable.getIntrinsicHeight() / (float) mThisHeight;
        float scale = Math.max(fw, fh) * 8;

        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "computeMaxZoom: " + scale);
        }
        return scale;
    }

    private float computeMinZoom() {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "computeMinZoom");
        }

        final Drawable drawable = getDrawable();

        if (drawable == null) {
            return 1F;
        }

        float scale = getScale(mBaseMatrix);
        scale = Math.min(1f, 1f / scale);

        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "computeMinZoom: " + scale);
        }

        return scale;
    }

    float getMaxScale() {
        if (mMaxZoom == ZOOM_INVALID) {
            mMaxZoom = computeMaxZoom();
        }
        return mMaxZoom;
    }

    float getMinScale() {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "getMinScale, mMinZoom: " + mMinZoom);
        }

        if (mMinZoom == ZOOM_INVALID) {
            mMinZoom = computeMinZoom();
        }

        if (LOG_ENABLED) {
            Log.v(LOG_TAG, "mMinZoom: " + mMinZoom);
        }

        return mMinZoom;
    }

    private Matrix getImageViewMatrix() {
        return getImageViewMatrix(mSuppMatrix);
    }

    private Matrix getImageViewMatrix(Matrix supportMatrix) {
        mDisplayMatrix.set(mBaseMatrix);
        mDisplayMatrix.postConcat(supportMatrix);
        return mDisplayMatrix;
    }

    private void getProperBaseMatrix(Drawable drawable, Matrix matrix) {
        float viewWidth = mThisWidth;
        float viewHeight = mThisHeight;

        if (LOG_ENABLED) {
            Log.d(LOG_TAG, "getProperBaseMatrix. view: " + viewWidth + "x" + viewHeight);
        }

        float w = drawable.getIntrinsicWidth();
        float h = drawable.getIntrinsicHeight();
        float widthScale, heightScale;
        matrix.reset();

        if (w > viewWidth || h > viewHeight) {
            widthScale = viewWidth / w;
            heightScale = viewHeight / h;
            float scale = Math.min(widthScale, heightScale);
            matrix.postScale(scale, scale);

            float tw = (viewWidth - w * scale) / 2.0f;
            float th = (viewHeight - h * scale) / 2.0f;
            matrix.postTranslate(tw, th);

        }
        else {
            widthScale = viewWidth / w;
            heightScale = viewHeight / h;
            float scale = Math.min(widthScale, heightScale);
            matrix.postScale(scale, scale);

            float tw = (viewWidth - w * scale) / 2.0f;
            float th = (viewHeight - h * scale) / 2.0f;
            matrix.postTranslate(tw, th);
        }

        if (LOG_ENABLED) {
            printMatrix(matrix);
        }
    }

 /*   protected void getProperBaseMatrix2(Drawable bitmap, Matrix matrix) {

        float viewWidth = mThisWidth;
        float viewHeight = mThisHeight;

        float w = bitmap.getIntrinsicWidth();
        float h = bitmap.getIntrinsicHeight();

        matrix.reset();

        float widthScale = viewWidth / w;
        float heightScale = viewHeight / h;

        float scale = Math.min(widthScale, heightScale);

        matrix.postScale(scale, scale);
        matrix.postTranslate((viewWidth - w * scale) / 2.0f, (viewHeight - h * scale) / 2.0f);
    }*/

    private float getValue(Matrix matrix, int whichValue) {
        matrix.getValues(mMatrixValues);
        return mMatrixValues[whichValue];
    }

    private void printMatrix(Matrix matrix) {
        float scaleX = getValue(matrix, Matrix.MSCALE_X);
        float scaleY = getValue(matrix, Matrix.MSCALE_Y);
        float tx = getValue(matrix, Matrix.MTRANS_X);
        float ty = getValue(matrix, Matrix.MTRANS_Y);
        Log.d(LOG_TAG, "matrix: { x: " + tx + ", y: " + ty + ", scalex: " + scaleX + ", scaley: " + scaleY + " }");
    }

    private RectF getBitmapRect() {
        return getBitmapRect(mSuppMatrix);
    }

    private RectF getBitmapRect(Matrix supportMatrix) {
        final Drawable drawable = getDrawable();

        if (drawable == null) return null;
        Matrix m = getImageViewMatrix(supportMatrix);
        mBitmapRect.set(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        m.mapRect(mBitmapRect);
        return mBitmapRect;
    }

    private float getScale(Matrix matrix) {
        return getValue(matrix, Matrix.MSCALE_X);
    }

    @SuppressLint ("Override")
    public float getRotation() {
        return 0;
    }

    float getScale() {
        return getScale(mSuppMatrix);
    }

//    public float getBaseScale() {
//        return getScale(mBaseMatrix);
//    }

    private void center() {
        final Drawable drawable = getDrawable();
        if (drawable == null) return;

        RectF rect = getCenter(mSuppMatrix);

        if (rect.left != 0 || rect.top != 0) {

            if (LOG_ENABLED) {
                Log.i(LOG_TAG, "center");
            }
            postTranslate(rect.left, rect.top);
        }
    }

    private RectF getCenter(Matrix supportMatrix) {
        final Drawable drawable = getDrawable();

        if (drawable == null) return new RectF(0, 0, 0, 0);

        mCenterRect.set(0, 0, 0, 0);
        RectF rect = getBitmapRect(supportMatrix);
        float height;
        if (rect != null) {
            height = rect.height();
            float width = rect.width();
            float deltaX = 0, deltaY = 0;
            int viewHeight = mThisHeight;
            if (height < viewHeight) {
                deltaY = (viewHeight - height) / 2 - rect.top;
            }
            else if (rect.top > 0) {
                deltaY = - rect.top;
            }
            else if (rect.bottom < viewHeight) {
                deltaY = mThisHeight - rect.bottom;
            }
            int viewWidth = mThisWidth;
            if (width < viewWidth) {
                deltaX = (viewWidth - width) / 2 - rect.left;
            }
            else if (rect.left > 0) {
                deltaX = - rect.left;
            }
            else if (rect.right < viewWidth) {
                deltaX = viewWidth - rect.right;
            }
            mCenterRect.set(deltaX, deltaY, 0, 0);
        }
        return mCenterRect;
    }

    private void postTranslate(float deltaX, float deltaY) {
        if (deltaX != 0 || deltaY != 0) {
            if (LOG_ENABLED) {
                Log.i(LOG_TAG, "postTranslate: " + deltaX + "x" + deltaY);
            }
            mSuppMatrix.postTranslate(deltaX, deltaY);
            setImageMatrix(getImageViewMatrix());
        }
    }

    private void postScale(float scale, float centerX, float centerY) {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "postScale: " + scale + ", center: " + centerX + "x" + centerY);
        }
        mSuppMatrix.postScale(scale, scale, centerX, centerY);
        setImageMatrix(getImageViewMatrix());
    }

    private PointF getCenter() {
        return mCenter;
    }

    void zoomTo(float scale) {
        if (LOG_ENABLED) {
            Log.i(LOG_TAG, "zoomTo: " + scale);
        }

        if (scale > getMaxScale()) scale = getMaxScale();
        if (scale < getMinScale()) scale = getMinScale();

        if (LOG_ENABLED) {
            Log.d(LOG_TAG, "sanitized scale: " + scale);
        }


        PointF center = getCenter();
        zoomTo(scale, center.x, center.y);
    }

    void zoomTo(float scale, float centerX, float centerY) {
        if (scale > getMaxScale()) scale = getMaxScale();

        float oldScale = getScale();
        float deltaScale = scale / oldScale;
        postScale(deltaScale, centerX, centerY);
        center();
    }

    void onZoomAnimationCompleted(float scale) {}

    private void panBy(double dx, double dy) {
        RectF rect = getBitmapRect();
        mScrollRect.set((float) dx, (float) dy, 0, 0);
        updateRect(rect, mScrollRect);
        postTranslate(mScrollRect.left, mScrollRect.top);
        center();
    }

    private void updateRect(RectF bitmapRect, RectF scrollRect) {
        if (bitmapRect == null) return;

        if (bitmapRect.top >= 0 && bitmapRect.bottom <= mThisHeight) scrollRect.top = 0;
        if (bitmapRect.left >= 0 && bitmapRect.right <= mThisWidth) scrollRect.left = 0;
        if (bitmapRect.top + scrollRect.top >= 0 && bitmapRect.bottom > mThisHeight) scrollRect.top = (int) (0 - bitmapRect.top);
        if (bitmapRect.bottom + scrollRect.top <= (mThisHeight) && bitmapRect.top < 0) scrollRect.top = (int) ((mThisHeight) - bitmapRect.bottom);
        if (bitmapRect.left + scrollRect.left >= 0) scrollRect.left = (int) (0 - bitmapRect.left);
        if (bitmapRect.right + scrollRect.left <= (mThisWidth)) scrollRect.left = (int) ((mThisWidth) - bitmapRect.right);
    }

    void scrollBy(float distanceX, float distanceY) {
        final double dx = distanceX;
        final double dy = distanceY;
        final long startTime = System.currentTimeMillis();
        mHandler.post(
                new Runnable() {

                    double old_x = 0;
                    double old_y = 0;

                    @Override
                    public void run() {
                        long now = System.currentTimeMillis();
                        double currentMs = Math.min((double) 300, now - startTime);
                        double x = mEasing.easeOut(currentMs, 0, dx, (double) 300);
                        double y = mEasing.easeOut(currentMs, 0, dy, (double) 300);
                        panBy((x - old_x), (y - old_y));
                        old_x = x;
                        old_y = y;
                        if (currentMs < (double) 300) {
                            mHandler.post(this);
                        }
                        else {
                            RectF centerRect = getCenter(mSuppMatrix);
                            if (centerRect.left != 0 || centerRect.top != 0) scrollBy(centerRect.left, centerRect.top);
                        }
                    }
                }
        );
    }

    void zoomTo(float scale, float centerX, float centerY, final float durationMs) {
        if (scale > getMaxScale()) scale = getMaxScale();

        final long startTime = System.currentTimeMillis();
        final float oldScale = getScale();

        final float deltaScale = scale - oldScale;

        Matrix m = new Matrix(mSuppMatrix);
        m.postScale(scale, scale, centerX, centerY);
        RectF rect = getCenter(m);

        final float destX = centerX + rect.left * scale;
        final float destY = centerY + rect.top * scale;

        mHandler.post(
                new Runnable() {

                    @Override
                    public void run() {
                        long now = System.currentTimeMillis();
                        float currentMs = Math.min(durationMs, now - startTime);
                        float newScale = (float) mEasing.easeInOut(currentMs, 0, deltaScale, durationMs);
                        zoomTo(oldScale + newScale, destX, destY);
                        if (currentMs < durationMs) {
                            mHandler.post(this);
                        }
                        else {
                            onZoomAnimationCompleted(getScale());
                            center();
                        }
                    }
                }
        );
    }
}
