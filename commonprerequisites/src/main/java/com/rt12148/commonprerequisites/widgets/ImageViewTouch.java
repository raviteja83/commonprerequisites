package com.rt12148.commonprerequisites.widgets;

/**
 * Created by ateendra on 5/22/15.
 **/

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;

public class ImageViewTouch extends ImageViewTouchBase {

    private ScaleGestureDetector mScaleDetector;
    private GestureDetector mGestureDetector;
    private float mScaleFactor;
    private int mDoubleTapDirection;

    public ImageViewTouch(Context context) {
        super(context);
    }

    public ImageViewTouch(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageViewTouch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void init(Context context, AttributeSet attrs, int defStyle) {
        super.init(context, attrs, defStyle);
        OnGestureListener mGestureListener = getGestureListener();
        OnScaleGestureListener mScaleListener = getScaleListener();

        mScaleDetector = new ScaleGestureDetector(getContext(), mScaleListener);
        mGestureDetector = new GestureDetector(getContext(), mGestureListener, null, true);

        mDoubleTapDirection = 1;
    }

    private OnGestureListener getGestureListener() {
        return new GestureListener();
    }

    private OnScaleGestureListener getScaleListener() {
        return new ScaleListener();
    }

    @Override
    protected void _setImageDrawable(final Drawable drawable,
                                     final Matrix initial_matrix,
                                     float min_zoom, float max_zoom) {
        super._setImageDrawable(drawable, initial_matrix, min_zoom, max_zoom);
        mScaleFactor = getMaxScale() / 3;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(getBitmapChanged()) return false;
        mScaleDetector.onTouchEvent(event);

        if (! mScaleDetector.isInProgress()) {
            mGestureDetector.onTouchEvent(event);
        }

        int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                return onUp();
        }
        if (getScale() > 1f) {
            getParent().requestDisallowInterceptTouchEvent(true);
        } else {
            getParent().requestDisallowInterceptTouchEvent(false);
        }
        return true;
    }


    @Override
    protected void onZoomAnimationCompleted(float scale) {

        if (LOG_ENABLED) {
            Log.d(LOG_TAG, "onZoomAnimationCompleted. scale: " + scale + ", minZoom: " + getMinScale());
        }

        if (scale < getMinScale()) {
            zoomTo(getMinScale());
        }
    }

    private float onDoubleTapPost(float scale, float maxZoom) {
        if (mDoubleTapDirection == 1) {
            if ((scale + (mScaleFactor * 2)) <= maxZoom) {
                return scale + mScaleFactor;
            }
            else {
                mDoubleTapDirection = - 1;
                return maxZoom;
            }
        }
        else {
            mDoubleTapDirection = 1;
            return 1f;
        }
    }


    private boolean onScroll(float distanceX, float distanceY) {
        if (getScale() == 1f) return false;
        mUserScaled = true;
        scrollBy(- distanceX, - distanceY);
        invalidate();
        return true;
    }

    private boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float diffX = e2.getX() - e1.getX();
        float diffY = e2.getY() - e1.getY();

        if (Math.abs(velocityX) > 800 || Math.abs(velocityY) > 800) {
            mUserScaled = true;
            scrollBy(diffX / 2, diffY / 2);
            invalidate();
            return true;
        }
        return false;
    }

    private boolean onDown() {
        return !getBitmapChanged();
    }

    private boolean onUp() {
        if (getBitmapChanged()) return false;
        if (getScale() < getMinScale()) {
            zoomTo(getMinScale());
        }
        return true;
    }

    private boolean onSingleTapUp() {
        return !getBitmapChanged();
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
        return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (LOG_ENABLED) {
                Log.i(LOG_TAG, "onDoubleTap. double tap enabled? " + true);
            }
            mUserScaled = true;
            float scale = getScale();
            float targetScale;
            if(scale > 1.0f){
                targetScale = 1.0f;
            }else {
                targetScale = onDoubleTapPost(scale, getMaxScale());
                targetScale = Math.min(getMaxScale(), Math.max(targetScale, getMinScale()));
            }
            zoomTo(targetScale, e.getX(), e.getY(), DEFAULT_ANIMATION_DURATION);
            invalidate();
            return super.onDoubleTap(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            if (isLongClickable()) {
                if (! mScaleDetector.isInProgress()) {
                    setPressed(true);
                    performLongClick();
                }
            }
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return !(e1 == null || e2 == null) && !(e1.getPointerCount() > 1 || e2.getPointerCount() > 1) && !mScaleDetector.isInProgress() && ImageViewTouch.this.onScroll(distanceX, distanceY);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return !(e1.getPointerCount() > 1 || e2.getPointerCount() > 1) && !mScaleDetector.isInProgress() && getScale() != 1f && ImageViewTouch.this.onFling(e1, e2, velocityX, velocityY);
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return ImageViewTouch.this.onSingleTapUp();
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return ImageViewTouch.this.onDown();
        }
    }


    public class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        boolean mScaled = false;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float span = detector.getCurrentSpan() - detector.getPreviousSpan();
            float targetScale = getScale() * detector.getScaleFactor();

            if (mScaled && span != 0) {
                mUserScaled = true;
                targetScale = Math.min(getMaxScale(), Math.max(targetScale, getMinScale() - 0.1f));
                zoomTo(targetScale, detector.getFocusX(), detector.getFocusY());
                mDoubleTapDirection = 1;
                invalidate();
                return true;
            }

            // This is to prevent a glitch the first time
            // image is scaled.
            if (!mScaled) mScaled = true;
            return true;
        }

    }
}
