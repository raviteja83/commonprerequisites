package com.rt12148.commonprerequisites.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rt12148.commonprerequisites.R;


/**
 * Created by Ravi-PC on 18-01-2016.
 **/

public class BadgeView extends TextView {

    private boolean mHideOnNull = true;
    private boolean stroke = false;

    public BadgeView(Context context) {
        this(context, null);
    }

    public BadgeView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public BadgeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if(getLayoutParams() == null) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.TOP | Gravity.START;
        }

        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.BadgeView);
        int textColor = array.getColor(R.styleable.BadgeView_badge_text_color, Color.WHITE);
        int bgColor = array.getColor(R.styleable.BadgeView_badge_bg_color, Color.TRANSPARENT);
        int borderColor = array.getColor(R.styleable.BadgeView_badge_border_color, Color.parseColor("#444444"));
        array.recycle();

        // set default font
        setTextColor(textColor);
        setTypeface(Typeface.DEFAULT_BOLD);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        setGravity(Gravity.CENTER);
        setWidth(dip2Px());
        setHeight(dip2Px());
        // set default background
        if(bgColor != Color.TRANSPARENT) {
            setBackground(bgColor);
            stroke = false;
        }else{
            stroke = true;
            setBackground(borderColor);
        }
        // default values
        setHideOnNull();
        setBadgeCount(0);
    }

    private void setBackground(int badgeColor) {
        ShapeDrawable bgDrawable = new ShapeDrawable(new OvalShape());
        bgDrawable.setIntrinsicWidth(24);
        bgDrawable.setIntrinsicHeight(24);
        bgDrawable.getPaint().setColor(badgeColor);
        if(stroke) {
            bgDrawable.getPaint().setStyle(Paint.Style.STROKE);
        }else {
            bgDrawable.getPaint().setStyle(Paint.Style.FILL);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(bgDrawable);
        } else {
            //noinspection deprecation
            setBackgroundDrawable(bgDrawable);
        }
    }

    /**
     * @return Returns true if view is hidden on badge value 0 or null;
     */
    private boolean isHideOnNull() {
        return mHideOnNull;
    }

    /**
     */
    private void setHideOnNull() {
        mHideOnNull = true;
        setText(getText());
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.TextView#setText(java.lang.CharSequence, android.widget.TextView.BufferType)
     */
    @Override
    public void setText(CharSequence text, BufferType type) {
        if (isHideOnNull() && (text == null || text.toString().equalsIgnoreCase("0"))) {
            setVisibility(View.GONE);
        } else {
            setVisibility(View.VISIBLE);
        }
        super.setText(text, type);
    }

    public void setBadgeCount(int count) {
        setText(String.valueOf(count));
    }

    /*public Integer getBadgeCount() {
        if (getText() == null) {
            return null;
        }

        String text = getText().toString();
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return null;
        }
    }*/

    /*
    public void setBadgeGravity(int gravity) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
        params.gravity = gravity;
        setLayoutParams(params);
    }

    public int getBadgeGravity() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
        return params.gravity;
    }

    public void setBadgeMargin(int dipMargin) {
        setBadgeMargin(dipMargin, dipMargin, dipMargin, dipMargin);
    }

    public void setBadgeMargin(int leftDipMargin, int topDipMargin, int rightDipMargin, int bottomDipMargin) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
        params.leftMargin = dip2Px(leftDipMargin);
        params.topMargin = dip2Px(topDipMargin);
        params.rightMargin = dip2Px(rightDipMargin);
        params.bottomMargin = dip2Px(bottomDipMargin);
        setLayoutParams(params);
    }

    public int[] getBadgeMargin() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
        return new int[]{params.leftMargin, params.topMargin, params.rightMargin, params.bottomMargin};
    }

    public void incrementBadgeCount(int increment) {
        Integer count = getBadgeCount();
        if (count == null) {
            setBadgeCount(increment);
        } else {
            setBadgeCount(increment + count);
        }
    }

    public void decrementBadgeCount(int decrement) {
        incrementBadgeCount(-decrement);
    }

    public void setTargetView(SlidingTabStrip target, int tabIndex) {
        View tabView = target.getChildAt(tabIndex);
        setTargetView(tabView);
    }
    public void setTargetView(View target) {
        if (getParent() != null) {
            ((ViewGroup) getParent()).removeView(this);
        }

        if (target == null) {
            return;
        }

        if (target instanceof LinearLayout) {
            LinearLayout LinearLayout =  ((LinearLayout) target);
            for(int i= 0;i < LinearLayout.getChildCount();i++) {
                View view = LinearLayout.getChildAt(i);
                if(view instanceof BadgeView){
                    LinearLayout.removeViewAt(i);
                }
            }
            ((LinearLayout) target).addView(this);

        } else if (target.getParent() instanceof ViewGroup) {
            ViewGroup parentContainer = (ViewGroup) target.getParent();
            int groupIndex = parentContainer.indexOfChild(target);
            parentContainer.removeView(target);

            LinearLayout badgeContainer = new LinearLayout(getContext());
            ViewGroup.LayoutParams parentLayoutParams = target.getLayoutParams();

            badgeContainer.setLayoutParams(parentLayoutParams);
            target.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            parentContainer.addView(badgeContainer, groupIndex, parentLayoutParams);
            badgeContainer.addView(target);

            badgeContainer.addView(this);
        } else if (target.getParent() == null) {
            Log.e(getClass().getSimpleName(), "ParentView is needed");
        }

    }
   */

    /**
     * converts dip to px
     **/
    private int dip2Px() {
        return (int) ((float) 20 * getContext().getResources().getDisplayMetrics().density + 0.5f);
    }

}


