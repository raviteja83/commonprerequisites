package com.rt12148.commonprerequisites.widgets;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

class FastBitmapDrawable extends Drawable {

    private final Bitmap mBitmap;
    private final Paint mPaint;
    private final int mIntrinsicWidth;
    private final int mIntrinsicHeight;

    FastBitmapDrawable(Bitmap b) {
        mBitmap = b;
        if( null != mBitmap ) {
            mIntrinsicWidth = mBitmap.getWidth();
            mIntrinsicHeight = mBitmap.getHeight();
        } else {
            mIntrinsicWidth = 0;
            mIntrinsicHeight = 0;
        }
        mPaint = new Paint();
        mPaint.setDither( true );
        mPaint.setFilterBitmap( true );
    }

    @Override
    public void draw(@NonNull Canvas canvas ) {
        if( null != mBitmap && !mBitmap.isRecycled() ) {
            canvas.drawBitmap( mBitmap, 0.0f, 0.0f, mPaint );
        }
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setAlpha( int alpha ) {
        mPaint.setAlpha( alpha );
    }

    @Override
    public void setColorFilter( ColorFilter cf ) {
        mPaint.setColorFilter( cf );
    }

    @Override
    public int getIntrinsicWidth() {
        return mIntrinsicWidth;
    }

    @Override
    public int getIntrinsicHeight() {
        return mIntrinsicHeight;
    }

    @Override
    public int getMinimumWidth() {
        return mIntrinsicWidth;
    }

    @Override
    public int getMinimumHeight() {
        return mIntrinsicHeight;
    }

}
