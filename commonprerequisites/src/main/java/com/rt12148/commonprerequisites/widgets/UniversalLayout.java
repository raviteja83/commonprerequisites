package com.rt12148.commonprerequisites.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rt12148.commonprerequisites.BuildConfig;
import com.rt12148.commonprerequisites.R;


@SuppressWarnings("unused")
public class UniversalLayout extends FrameLayout{

    private final String TAG = getClass().getSimpleName();
    private ImageView mImageView;
    private ProgressWheel progressBar;
    private View mEmptyView;
    private ViewStub mEmpty,mImageStub;
    private TextView mEmptyMessage;
    public Button mRetry;
    public RecyclerView mRecyclerView;
    public SwipeRefreshLayout mSwipeRefreshLayout;

    private RecyclerView.Adapter mAdapter;

    private OnRetryClickListener retryClickListener;

    private int mEmptyId = 0;
    private int mImageId = 0;
    private int mDrawableTop = 0;
    private int mRetryTextColor = 0;
    private int mRetryBgColor = 0;
    private float mRetryTextSize = 0;
    private int mDefaultEmptyMessage = 0;
    private  String defaultEmptyMessage = "";

    private boolean showOnlyImage = false;
    private boolean enableSwipeRefresh = true;

    private OnSwipeRefreshListener swipeRefreshListener;

    public UniversalLayout(Context context) {
        this(context,null);
    }

    public UniversalLayout(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public UniversalLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context,attrs,defStyleAttr);
        initAttrs(attrs);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UniversalLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttrs(attrs);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.universal_layout,this,true);
        mImageStub = (ViewStub) view.findViewById(R.id.universal_imageView);
        progressBar = (ProgressWheel) view.findViewById(R.id.universal_progress);
        mEmpty = (ViewStub) view.findViewById(R.id.universal_emptyView);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.universal_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.universal_swipe_refresh);

        mSwipeRefreshLayout.setEnabled(!showOnlyImage);
        if(showOnlyImage){
            mEmpty.setVisibility(GONE);
        }else {
            setEmptyView(mEmptyId);
        }

        mSwipeRefreshLayout.setEnabled(enableSwipeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (null != swipeRefreshListener) {
                    swipeRefreshListener.onRefreshed();
                }
            }
        });
    }

    protected void initAttrs(AttributeSet attrs) {
        if(attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.UniversalLayout);
            try {
                mEmptyId = typedArray.getResourceId(R.styleable.UniversalLayout_empty_view, R.layout.empty_view);
                mImageId = typedArray.getResourceId(R.styleable.UniversalLayout_image_view, 0);
                mRetryTextSize = typedArray.getDimension(R.styleable.UniversalLayout_retry_text_size, 0);
                mRetryTextColor = typedArray.getColor(R.styleable.UniversalLayout_retry_text_color, Color.WHITE);
                mRetryBgColor = typedArray.getResourceId(R.styleable.UniversalLayout_retry_bg_color, R.color.myAccentColor);
                showOnlyImage = typedArray.getBoolean(R.styleable.UniversalLayout_show_only_image, false);
                enableSwipeRefresh = typedArray.getBoolean(R.styleable.UniversalLayout_enable_swipe_refresh, false);
                mDrawableTop = typedArray.getResourceId(R.styleable.UniversalLayout_empty_view_image, 0);
                mDefaultEmptyMessage = typedArray.getResourceId(R.styleable.UniversalLayout_empty_view_default_message, 0);
            } finally {
                typedArray.recycle();
            }
        }
    }


    public void setEmptyMessage(String message){
        if(mEmptyMessage != null){
            mEmptyMessage.setText(message);
        }
    }

    public void setEmptyMessage(@StringRes int resId){
        try {
            if (mEmptyMessage != null && resId != 0)
                mEmptyMessage.setText(resId);
        }catch (Resources.NotFoundException e){
            if(BuildConfig.DEBUG){
                Log.e(TAG,e.getMessage());
            }
        }
    }

    public void setDefaultEmptyMessage(@StringRes int resId){
        try {
            if (mEmptyMessage != null && resId != 0){
                mDefaultEmptyMessage = resId;
                mEmptyMessage.setText(resId);
            }
        }catch (Resources.NotFoundException e){
            if(BuildConfig.DEBUG){
                Log.e(TAG,e.getMessage());
            }
            mDefaultEmptyMessage = 0;
        }
    }

    public void setDefaultEmptyMessage(String message){
        if (mEmptyMessage != null && message != null && !message.isEmpty()){
            defaultEmptyMessage = message;
            mEmptyMessage.setText(message);
        }else{
            defaultEmptyMessage = "";
        }
    }

    public void setDefaultDrawableTop(@DrawableRes int resId) {
        if(mEmptyMessage != null ){
            try {
                mDrawableTop = resId;
                Drawable drawable = ContextCompat.getDrawable(getContext(), mDrawableTop);
                setDrawableTop(drawable);
            }catch (Resources.NotFoundException e){
                if(BuildConfig.DEBUG){
                    Log.e(TAG,e.getMessage());
                }
                mDrawableTop = 0;
            }
        }
    }

    public void setDrawableTop(@DrawableRes int resId){
        if(mEmptyMessage != null ){
            try {
                if (resId == 0) {
                    resId = mDrawableTop;
                }
                Drawable drawable = ContextCompat.getDrawable(getContext(), resId);
                setDrawableTop(drawable);
            }catch (Resources.NotFoundException e){
                if(BuildConfig.DEBUG){
                    Log.e(TAG,e.getMessage());
                }
            }
        }
    }

    public void setDrawableTop(Drawable drawableTop){
        if(mEmptyMessage != null){
            mEmptyMessage.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);
        }
    }

    public void showProgress(){
        progressBar.setVisibility(VISIBLE);
        mRecyclerView.setVisibility(GONE);
        showHideEmptyView(true);
    }

    public void hideProgress(boolean error){
        progressBar.setVisibility(GONE);
        toggleEmptyView();
        mRetry.setVisibility(error ? VISIBLE : GONE);
        mRecyclerView.setVisibility(error ? GONE : VISIBLE);
    }

    public void setImageResource(@DrawableRes int resource) {
        try {
            if(mImageView != null) {
                mImageView.setImageResource(resource);
            }
        }catch (Resources.NotFoundException e){
            if(BuildConfig.DEBUG){
                Log.e(TAG,e.getMessage());
            }
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        if(mImageView != null) {
            mImageView.setImageBitmap(bitmap);
        }
    }

    public void setImageDrawable(Drawable drawable) {
        if(mImageView != null) {
            mImageView.setImageDrawable(drawable);
        }
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public void setImageView(@LayoutRes int resId) {
        mImageId = resId;
        mImageStub.setLayoutResource(mEmptyId);
        View view;
        if (mImageId != 0) {
            view = mImageStub.inflate();
            if (view != null && view instanceof ImageView) {
                mImageView = (ImageView) mImageStub.inflate();
            }else{
                throw new InflateException("Make ImageView element as root of your imageView layout");
            }
        }
        mImageStub.setVisibility(View.GONE);
    }

    public void setEmptyView(@LayoutRes int emptyResourceId) {
        mEmptyId = emptyResourceId;
        mEmpty.setLayoutResource(mEmptyId);
        if (mEmptyId != 0)
            mEmptyView = mEmpty.inflate();

        mEmpty.setVisibility(View.GONE);

        if(mEmptyView != null){
            mEmptyMessage = (TextView) mEmptyView.findViewWithTag("emptyMessage");
            if(mEmptyView == null){
                throw new IllegalStateException("Specify a textView with tag emptyMessage in your emptyView Layout");
            }
            mRetry = (Button) mEmptyView.findViewById(R.id.emptyView_retry_btn);
            if(mRetry != null) {
                mRetry.setBackgroundResource(mRetryBgColor);
                mRetry.setTextColor(mRetryTextColor);
                if(mRetryTextSize == 0){
                    mRetry.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                }else{
                    mRetry.setTextSize(mRetryTextSize);
                }
                mRetry.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (retryClickListener != null) {
                            retryClickListener.OnRetry(v);
                        }
                    }
                });
            }
            if(mDrawableTop != 0)
                setDrawableTop(mDrawableTop);
            if(mDefaultEmptyMessage != 0)
                setEmptyMessage(mDefaultEmptyMessage);
        }
    }

    public interface OnRetryClickListener{
        void OnRetry(View v);
    }

    public void setRetryClickListener(OnRetryClickListener retryClickListener) {
        this.retryClickListener = retryClickListener;
    }

    public void showEmptyDefaults() {
        showHideEmptyView(false);
        hideProgress(false);
        setDrawableTop(mDrawableTop);
        mRetry.setVisibility(GONE);
        if(mDefaultEmptyMessage != 0) {
            setEmptyMessage(mDefaultEmptyMessage);
        }else {
            if (defaultEmptyMessage != null && !defaultEmptyMessage.isEmpty()){
                setEmptyMessage(defaultEmptyMessage);
            }
        }
    }

    public void showHideEmptyView(boolean hide){
        if (mEmptyId != 0)
            mEmpty.setVisibility(hide ? GONE : VISIBLE);
    }

    public void addItemDividerDecoration(Context context) {
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(context);
        mRecyclerView.addItemDecoration(itemDecoration);
    }

    public void addItemDecoration(RecyclerView.ItemDecoration itemDecoration) {
        mRecyclerView.addItemDecoration(itemDecoration);
    }

    public void addItemDecoration(RecyclerView.ItemDecoration itemDecoration, int index) {
        mRecyclerView.addItemDecoration(itemDecoration, index);
    }

    public void setItemAnimator(RecyclerView.ItemAnimator animator) {
        mRecyclerView.setItemAnimator(animator);
    }

    public RecyclerView.ItemAnimator getItemAnimator() {
        return mRecyclerView.getItemAnimator();
    }


    public void setLayoutManager(RecyclerView.LayoutManager manager) {
        mRecyclerView.setLayoutManager(manager);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        if(adapter == null) return;
        mAdapter = adapter;
        if(mRecyclerView.getLayoutManager() == null){
            throw new IllegalStateException("Layout Manager not attached");
        }
        mRecyclerView.setAdapter(mAdapter);
        /**
         * set dataSet observer to show or hide empty view on items added or removed or changed
         */
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                toggleEmptyView();
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                toggleEmptyView();
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                toggleEmptyView();
            }
        });
    }

    private void toggleEmptyView(){
        if(null == mAdapter){
            if(BuildConfig.DEBUG){
                Log.e(TAG,"set adapter using setAdapter() method");
            }
            return;
        }
        showHideEmptyView(mAdapter.getItemCount() != 0);
    }

    public RecyclerView.Adapter getAdapter() {
        return mRecyclerView.getAdapter();
    }


    public interface OnSwipeRefreshListener {
        void onRefreshed();
    }

    public void setSwipeRefreshListener(final OnSwipeRefreshListener swipeRefreshListener) {
        this.swipeRefreshListener = swipeRefreshListener;
    }

    public void setRefreshing(boolean refreshing){
        mSwipeRefreshLayout.setRefreshing(refreshing);
    }

    public boolean isRefreshing(){
        return mSwipeRefreshLayout.isRefreshing();
    }
}
